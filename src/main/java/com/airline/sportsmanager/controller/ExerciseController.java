package com.airline.sportsmanager.controller;

import com.airline.sportsmanager.entity.Member;
import com.airline.sportsmanager.model.CommonResult;
import com.airline.sportsmanager.model.ExerciseHistoryRequest;
import com.airline.sportsmanager.model.FinishTime;
import com.airline.sportsmanager.service.ExerciseHistoryService;
import com.airline.sportsmanager.service.MemberService;
import com.airline.sportsmanager.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/exercise")
public class ExerciseController {
    private final MemberService memberservice;
    private final ExerciseHistoryService exerciseHistoryService;
    @PostMapping("/history-id/{memberId}")
    public CommonResult setExerciseHistory(@PathVariable long memberId, @RequestBody @Valid ExerciseHistoryRequest request) {
        Member member = memberservice.getData(memberId);
        exerciseHistoryService.setExerciseHistory(member, request);
        return ResponseService.getSuccessResult();
    }
    @PutMapping("/finish/{memberId}")
    public CommonResult putTime(@PathVariable long memberId, @RequestBody @Valid FinishTime finishTime) {
        exerciseHistoryService.putTime(memberId, finishTime);

        return ResponseService.getSuccessResult();
    }
}
