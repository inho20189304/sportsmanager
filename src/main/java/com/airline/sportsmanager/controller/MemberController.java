package com.airline.sportsmanager.controller;

import com.airline.sportsmanager.model.*;
import com.airline.sportsmanager.service.ResponseService;
import com.airline.sportsmanager.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/data")
    public CommonResult setUser(@RequestBody @Valid MemberRequest register) {
        memberService.setUser(register);

        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all")
    public ListResult<MemberItem> getMembers() {
        return ResponseService.getListResult(memberService.getMembers(), true);
    }
    @PutMapping("/height-weight/{id}")
    public CommonResult putMember(@PathVariable long id, @RequestBody @Valid MemberInfoUpdateRequest request) {
        memberService.putMember(id, request);

        return ResponseService.getSuccessResult();
    }
}
