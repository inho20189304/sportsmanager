package com.airline.sportsmanager.controller;

import com.airline.sportsmanager.model.MemberInfoStatisticsResponse;
import com.airline.sportsmanager.model.MyExerciseResponse;
import com.airline.sportsmanager.model.SingleResult;
import com.airline.sportsmanager.service.MemberInfoStatisticsService;
import com.airline.sportsmanager.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/statistics")
public class MemberInfoStatistics {
    private final MemberInfoStatisticsService memberInfoStatisticsService;
    @GetMapping("/user")
    public SingleResult<MemberInfoStatisticsResponse> getStatistics() {
        return ResponseService.getSingleResult(memberInfoStatisticsService.getStatistics());
    }
    @GetMapping("/my/exercise/memberId")
    public SingleResult<MyExerciseResponse> getStatisticsByMy(@PathVariable long memberId) {
        return ResponseService.getSingleResult(memberInfoStatisticsService.getMyExercise(memberId));
    }
}
