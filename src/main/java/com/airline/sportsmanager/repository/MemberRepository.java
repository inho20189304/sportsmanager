package com.airline.sportsmanager.repository;

import com.airline.sportsmanager.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
