package com.airline.sportsmanager.repository;

import com.airline.sportsmanager.entity.ExerciseHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ExerciseHistoryRepository extends JpaRepository<ExerciseHistory, Long> {
    List<ExerciseHistory> findAllByIdOrderByIdDesc(long id);

    List<ExerciseHistory> findAllByMember_IdAndStartExerciseDate(long memberId, LocalDate searchDate);

}
