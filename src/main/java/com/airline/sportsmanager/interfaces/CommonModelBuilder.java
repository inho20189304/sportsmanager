package com.airline.sportsmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
