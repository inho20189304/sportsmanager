package com.airline.sportsmanager.model;

import com.airline.sportsmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyExerciseItem {
    private String exercisePartName;
    private Long runningTime;
    private Integer averageWeight;
    private Integer totalCount;

    private MyExerciseItem(MyExerciseItemBuilder builder) {
        this.exercisePartName = builder.exercisePartName;
        this.runningTime = builder.runningTime;
        this.averageWeight = builder.averageWeight;
        this.totalCount = builder.totalCount;
    }
    public static class MyExerciseItemBuilder implements CommonModelBuilder<MyExerciseItem> {
        private final String exercisePartName;
        private final Long runningTime;
        private final Integer averageWeight;
        private final Integer totalCount;

        public MyExerciseItemBuilder(String exercisePartName, Long runningTime, Integer averageWeight, Integer totalCount) {
            this.exercisePartName = exercisePartName;
            this.runningTime = runningTime;
            this.averageWeight = averageWeight;
            this.totalCount = totalCount;
        }
        @Override
        public MyExerciseItem build() {
            return new MyExerciseItem(this);
        }
    }
}
