package com.airline.sportsmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberRequest {
    @NotNull
    private String memberName;
    @NotNull
    private Boolean isMan;
    @NotNull
    private Double height;
    @NotNull
    private Double weight;

}
