package com.airline.sportsmanager.model;

import com.airline.sportsmanager.entity.Member;
import com.airline.sportsmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    private Boolean isMan;
    private Double height;
    private Double weight;
    private Double bmi;
    private String bmiResultName;

    private MemberItem(MemberItemBuilder builder) {
        this.isMan = builder.isMan;
        this.height = builder.height;
        this.weight = builder.weight;
        this.bmi = builder.bmi;
        this.bmiResultName = builder.bmiResultName;
    }
    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final Boolean isMan;
        private final Double height;
        private final Double weight;
        private final Double bmi;
        private final String bmiResultName;

        public MemberItemBuilder(Member member) {
            this.isMan = member.getIsMan();
            this.height = member.getHeight();
            this.weight = member.getWeight();
            this.bmi = member.getWeight() / ((member.getHeight() / 100) * (member.getHeight() / 100));

            String bmiResultName = "";

            if (this.bmi < 18.5) {
                bmiResultName = "저체중";
            } else if (bmi < 22.9) {
                bmiResultName = "정상";
            } else if (bmi < 24.9) {
                bmiResultName = "과체중";
            } else if (bmi < 29.9) {
                bmiResultName = "비만";
            } else {
                bmiResultName = "고도비만";
            }
            this.bmiResultName = bmiResultName;


        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
