package com.airline.sportsmanager.model;

import com.airline.sportsmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyExerciseResponse {
    private String runningTimeName;
    private String todayParts;
    private List<MyExerciseItem> exerciseItems;

    private MyExerciseResponse(MyExerciseResponseBuilder builder) {
        this.runningTimeName = builder.runningTimeName;
        this.todayParts = builder.todayParts;
        this.exerciseItems = builder.exerciseItems;
    }
    public static class MyExerciseResponseBuilder implements CommonModelBuilder<MyExerciseResponse> {
        private final String runningTimeName;
        private final String todayParts;
        private final List<MyExerciseItem> exerciseItems;

        public MyExerciseResponseBuilder(String runningTimeName, String todayParts, List<MyExerciseItem> exerciseItems) {
            this.runningTimeName = runningTimeName;
            this.todayParts = todayParts;
            this.exerciseItems = exerciseItems;
        }
        @Override
        public MyExerciseResponse build() {
            return new MyExerciseResponse(this);
        }
    }
}
