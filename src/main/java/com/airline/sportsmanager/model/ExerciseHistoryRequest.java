package com.airline.sportsmanager.model;

import com.airline.sportsmanager.enums.BodyParts;
import com.airline.sportsmanager.enums.Exercise;
import io.swagger.models.auth.In;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDate;
import java.time.LocalTime;


@Getter
@Setter
public class ExerciseHistoryRequest {
    private Exercise exercise;
    private BodyParts bodyParts;
    private Integer numberOfTime;
    private Integer reps;
    private Integer highestWeight;
    @Min(0)
    @Max(23)
    private Integer startExerciseTimeHour;
    @Min(0)
    @Max(59)
    private Integer startExerciseTimeMin;

}
