package com.airline.sportsmanager.model;

import io.swagger.models.auth.In;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Getter
@Setter
public class FinishTime {
    @Min(0)
    @Max(23)
    private Integer finishTimeHour;
    @Min(0)
    @Max(59)
    private Integer finishTimeMin;
}
