package com.airline.sportsmanager.model;

import com.airline.sportsmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.stream.events.Comment;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberInfoStatisticsResponse {
    private Long totalMemberCount;
    private Double averageHeight;
    private Double averageWeight;

    private MemberInfoStatisticsResponse(MemberInfoStatisticsResponseBuilder builder) {
        this.totalMemberCount = builder.totalMemberCount;
        this.averageHeight = builder.averageHeight;
        this.averageWeight = builder.averageWeight;
    }
    public static class MemberInfoStatisticsResponseBuilder implements CommonModelBuilder<MemberInfoStatisticsResponse>
    {
        private final Long totalMemberCount;
        private final Double averageHeight;
        private final Double averageWeight;

        public MemberInfoStatisticsResponseBuilder(
                Long totalMemberCount,
                Double averageHeight,
                Double averageWeight

        ){
            this.totalMemberCount = totalMemberCount;
            this.averageHeight = averageHeight;
            this.averageWeight = averageWeight;
        }
        @Override
        public MemberInfoStatisticsResponse build() {
            return new MemberInfoStatisticsResponse(this);
        }
    }
}
