package com.airline.sportsmanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberInfoUpdateRequest {
    @NotNull
    private Double height; // 변경할 값
    @NotNull
    private Double weight; // 변경할 값

}
