package com.airline.sportsmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Menu {

    CHICKENSALAD("닭가슴살 샐러드", "요요 방지 식단" ),
    PROTEIN("단백질 도시락", "벌크업 식단"),
    BROWNRICE("현미 도시락", "체지방 감소 식단"),
    EGGANDPOTATO("계란 감자 식단", "지방 감소 식단")
    ;

    private final String menuName;
    private final String purposeOfEat;

}
