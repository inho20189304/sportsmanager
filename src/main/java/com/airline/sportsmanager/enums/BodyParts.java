package com.airline.sportsmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BodyParts {
    ARM("팔"),
    CHEST("가슴"),
    LEGS("하체"),
    BACK("등")
    ;
    private final String bodyPartsName;
}
