package com.airline.sportsmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Exercise {
    SQUAT("스쿼트"),
    LEGPRESS("레그프레스"),
    LEGEXTENSION("레그익스텐션"),
    BENCHPRESS("벤치프레스"),
    CHESTPRESS("체스트프레스"),
    DIPS("딥스"),
    ARMPULLDOWN("암풀다운"),
    PULLUP("풀업"),
    LETPULLDOWN("렛풀다운"),
    DUMBELCULL("덤벨컬"),
    BABELCULL("바벨컬")
    ;
    private final String exerciseName;
}
