package com.airline.sportsmanager.entity;

import com.airline.sportsmanager.interfaces.CommonModelBuilder;
import com.airline.sportsmanager.model.MemberInfoUpdateRequest;
import com.airline.sportsmanager.model.MemberRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // ID
    private Long id;
    @Column(nullable = false, length = 20)
    private String memberName;
    @Column(nullable = false)
    private Boolean isMan; // 성별
    @Column(nullable = false)
    private Double height; // 키
    @Column(nullable = false)
    private Double weight; // 몸무게

    public void putMember(MemberInfoUpdateRequest request) {
        this.height = request.getHeight(); // 변경할 값
        this.weight = request.getWeight(); // 변경할 값
    }
    private Member(MemberBuilder builder) {
        this.memberName = builder.memberName;
        this.isMan = builder.isMan;
        this.height = builder.height;
        this.weight = builder.weight;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final String memberName;
        private final Boolean isMan;
        private final Double height;
        private final Double weight;

        public MemberBuilder(MemberRequest register) {
            this.memberName = register.getMemberName();
            this.isMan = register.getIsMan();
            this.height = register.getHeight();
            this.weight = register.getWeight();

        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }

}
