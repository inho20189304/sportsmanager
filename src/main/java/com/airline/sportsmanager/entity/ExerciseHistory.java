package com.airline.sportsmanager.entity;

import com.airline.sportsmanager.enums.BodyParts;
import com.airline.sportsmanager.enums.Exercise;
import com.airline.sportsmanager.interfaces.CommonModelBuilder;
import com.airline.sportsmanager.model.ExerciseHistoryRequest;
import com.airline.sportsmanager.model.FinishTime;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ExerciseHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //ID
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId")
    private Member member;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Exercise exerciseName;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private BodyParts bodyParts;
    @Column(nullable = false)
    private Integer numberOfTime;
    @Column(nullable = false)
    private Integer reps;
    @Column(nullable = false)
    private Integer highestWeight;
    @Column(nullable = false)
    private LocalDate startExerciseDate;
    @Column(nullable = false)
    private LocalTime startExerciseTime;
    private LocalTime finishExerciseTime;
    @Column(nullable = false)
    private Boolean isFinish;

    public void putFinishTime(FinishTime finishTime) {
        this.isFinish = true;
        this.finishExerciseTime = LocalTime.of(finishTime.getFinishTimeHour(), finishTime.getFinishTimeMin());
    }


    private ExerciseHistory(ExerciseHistoryBuilder builder) {
        this.member = builder.member;
        this.exerciseName = builder.exerciseName;
        this.bodyParts = builder.bodyParts;
        this.numberOfTime = builder.numberOfTime;
        this.reps = builder.reps;
        this.highestWeight = builder.highestWeight;
        this.startExerciseDate = builder.startExerciseDate;
        this.startExerciseTime = builder.startExerciseTime;
        this.finishExerciseTime = builder.finishExerciseTime;
        this.isFinish = builder.isFinish;
    }
    public static class ExerciseHistoryBuilder implements CommonModelBuilder<ExerciseHistory> {
        private final Member member;
        private final Exercise exerciseName;
        private final BodyParts bodyParts;
        private final Integer numberOfTime;
        private final Integer reps;
        private final Integer highestWeight;
        private final LocalDate startExerciseDate;
        private final LocalTime startExerciseTime;
        private final LocalTime finishExerciseTime;
        private final Boolean isFinish;

        public ExerciseHistoryBuilder(Member member, ExerciseHistoryRequest exerciseHistoryRequest) {
            this.member = member;
            this.exerciseName = exerciseHistoryRequest.getExercise();
            this.bodyParts = exerciseHistoryRequest.getBodyParts();
            this.numberOfTime = exerciseHistoryRequest.getNumberOfTime();
            this.reps = exerciseHistoryRequest.getReps();
            this.highestWeight = exerciseHistoryRequest.getHighestWeight();
            this.startExerciseDate = LocalDate.now();
            this.startExerciseTime = LocalTime.of(exerciseHistoryRequest.getStartExerciseTimeHour(), exerciseHistoryRequest.getStartExerciseTimeMin());
            this.finishExerciseTime = this.startExerciseTime;
            this.isFinish = false;
        }
        @Override
        public ExerciseHistory build() {
            return new ExerciseHistory(this);
        }
    }

}
