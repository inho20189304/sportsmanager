package com.airline.sportsmanager.service;

import com.airline.sportsmanager.entity.ExerciseHistory;
import com.airline.sportsmanager.entity.Member;
import com.airline.sportsmanager.model.MemberInfoStatisticsResponse;
import com.airline.sportsmanager.model.MyExerciseItem;
import com.airline.sportsmanager.model.MyExerciseResponse;
import com.airline.sportsmanager.repository.ExerciseHistoryRepository;
import com.airline.sportsmanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class MemberInfoStatisticsService {
    private final MemberRepository memberRepository;
    private final ExerciseHistoryRepository exerciseHistoryRepository;

    public MemberInfoStatisticsResponse getStatistics() {
        List<Member> originList = memberRepository.findAll();

        long totalMemberCount = originList.size();
        double totalHeight = 0;
        double totalWeight = 0;

        for (Member item : originList) {
            totalHeight += item.getHeight();
            totalWeight += item.getWeight();
        }

        return new MemberInfoStatisticsResponse.MemberInfoStatisticsResponseBuilder(
                totalMemberCount,
                totalHeight / totalMemberCount,
                totalWeight / totalMemberCount
        ).build();
    }

    public MyExerciseResponse getMyExercise(long memberId) {
        List<ExerciseHistory> originList = exerciseHistoryRepository.findAllByMember_IdAndStartExerciseDate(memberId, LocalDate.now());

        long totalRunningTime = 0;
        List<String> partNames = new LinkedList<>();
        List<MyExerciseItem> exerciseItems = new LinkedList<>();

        for (ExerciseHistory item : originList) {
            Duration duration = Duration.between(item.getStartExerciseTime(), item.getFinishExerciseTime());
            long second = duration.getSeconds();
            totalRunningTime += second / 60;

            partNames.add(item.getBodyParts().getBodyPartsName());

            MyExerciseItem addItem = new MyExerciseItem.MyExerciseItemBuilder(
                    item.getBodyParts().getBodyPartsName(),
                    second / 60,
                    item.getHighestWeight(),
                    item.getNumberOfTime() * item.getReps()
            ).build();

            exerciseItems.add(addItem);
        }

        Set<String> partNamesResult = new HashSet<>(partNames);
        String partNamesResultName = String.join("/", partNamesResult);
        return new MyExerciseResponse.MyExerciseResponseBuilder(totalRunningTime + "분", partNamesResultName,
                exerciseItems).build();

    }
}
