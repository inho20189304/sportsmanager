package com.airline.sportsmanager.service;

import com.airline.sportsmanager.entity.ExerciseHistory;
import com.airline.sportsmanager.entity.Member;
import com.airline.sportsmanager.exception.CMissingDataException;
import com.airline.sportsmanager.model.ExerciseHistoryRequest;
import com.airline.sportsmanager.model.FinishTime;
import com.airline.sportsmanager.model.MemberInfoUpdateRequest;
import com.airline.sportsmanager.repository.ExerciseHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ExerciseHistoryService {
    private final ExerciseHistoryRepository exerciseHistoryRepository;

    public void setExerciseHistory(Member member, ExerciseHistoryRequest exerciseHistoryRequest) {
        ExerciseHistory addData = new ExerciseHistory.ExerciseHistoryBuilder(member, exerciseHistoryRequest).build();

        exerciseHistoryRepository.save(addData);
    }

    public void putTime(long id, FinishTime finishTime) {
        ExerciseHistory originData = exerciseHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);
        originData.putFinishTime(finishTime);

        exerciseHistoryRepository.save(originData);
    }
}
