package com.airline.sportsmanager.service.nutri;

import com.airline.sportsmanager.entity.ExerciseHistory;
import com.airline.sportsmanager.model.ExerciseHistoryRequest;
import org.springframework.stereotype.Service;

import java.time.LocalTime;

@Service
public class TimeService {
    public void setTime(ExerciseHistoryRequest request) {
        LocalTime dateExerciseTime = LocalTime.of(request.getStartExerciseTimeHour(), request.getStartExerciseTimeMin());
    }
}
