package com.airline.sportsmanager.service;

import com.airline.sportsmanager.entity.Member;
import com.airline.sportsmanager.exception.CMissingDataException;
import com.airline.sportsmanager.model.ListResult;
import com.airline.sportsmanager.model.MemberInfoUpdateRequest;
import com.airline.sportsmanager.model.MemberItem;
import com.airline.sportsmanager.model.MemberRequest;
import com.airline.sportsmanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public void setUser(MemberRequest request) {
        Member addData = new Member.MemberBuilder(request).build();

        memberRepository.save(addData);
    }
    public ListResult<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new MemberItem.MemberItemBuilder(item).build() ));
        return ListConvertService.settingResult(result);
    }
    public void putMember(long id, MemberInfoUpdateRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        originData.putMember(request);

        memberRepository.save(originData);
    }

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }
}
